from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from influxdb import InfluxDBClient
from datetime import datetime, timedelta

measurement = []
week_days_mapper = {6: 0, 0: 1, 1: 2, 2: 3, 3: 4, 4: 5, 5: 6}

air_conditioners = {
    'klimatyzator_1': 30,
    'klimatyzator_2': 31,
}

influx = {
    'PORT': 8086,
    'PASSWORD': 'itg2xyCDPV2DyrS2zx',
    'USER': 'writer',
    'HOST': 'localhost',
    'DATABASE': 'data'
}

meters = {
    'licznik1': 70,
    'licznik2': 2,
    'licznik3': 3,
    'licznik4': 4,
    'licznik5': 5,
    'licznik6': 6,
    'licznik7': 7,
    'licznik8': 8,
    'licznik9': 9,
    'licznik10': 10,
    'licznik11': 11,
    'licznik12': 12,
    'licznik13': 13,
    'licznik14': 14,
    'licznik15': 15,
    'licznik19': 16,
    'licznik20': 17,
    'licznik21': 18,
    'licznik22': 21,
    'licznik23': 19,
    'licznik24': 20,
    'licznik27': 66,
    'licznik28': 67,
}

air_conditioners = {
    'klimatyzator_1': 30,
    'klimatyzator_2': 31,
}

thermometers = {
    'termometr_1': 64,
    'termometr_2': 65,
}

heatmeters = {
    'heatmeter_1': 1,
}


class InfluxClient():
    def __init__(self):
        self.client = InfluxDBClient(host=influx['HOST'], port=influx['PORT'], username=influx['USER'], password=influx['PASSWORD'],
                                   database=influx['DATABASE'])

    def write_series(self, series):
        self.client.write_points(series)
        print("Succesfully writed to influx DB")

    def get_schedule_from(self, measurement_name):
        pass

    def get_scheedule_to(self, measurement_name):
        pass

    def get_last_value_by_time(self, measurement_name, time):
        query = "select last(value) from \"{measurement}\" where time >= \'{time}\'".format(
            measurement=measurement_name, time=time)
        result = self.client.query(query)
        points = list(result.get_points())
        if len(points) > 0:
            return [points[0]['last']]
        return []

    def get_last_measurement_value(self, measurement_name):
        query = "select last(value) from \"{measurement}\"".format(measurement=measurement_name)
        result = self.client.query(query)
        points = list(result.get_points())
        if len(points) > 0:
            return [points[0]['last']]
        return []

    def get_schedule_value(self, measurement_name):
        day_num = week_days_mapper[datetime.now().weekday()]
        query = "select last(value) from \"{measurement}\" where \"Day\"='{day}'".format(measurement=measurement_name, day=day_num)
        result = self.client.query(query)
        points = list(result.get_points())
        if len(points) > 0:
            return [points[0]['last']]
        return []


class InfluxMeasurement:
    def __init__(self, measurement_name, value, tags={}):
        self.measurement = measurement_name
        self.fields = {
            "value": float(value)
        }
        self.tags = tags
        self.time = datetime.strftime(datetime.utcnow(), '%Y-%m-%dT%H:%M:%SZ')


def append_to_series(measurement_name, value):
    global measurement
    print(measurement_name, value)
    measurement.append(vars(InfluxMeasurement(measurement_name, value)))


def write_to_influx():
    global measurement
    if len(measurement) == 0:
        return
    inf = InfluxClient()
    inf.write_series(measurement)
    measurement = []


def time_in_range(start, end, x):
    """Return true if x is in the range [start, end]"""
    if start <= end:
        return start <= x <= end
    else:
        return start <= x or x <= end


for name in meters:
    client = ModbusClient(method='rtu', port='/dev/ttyUSB0', timeout=0.3, baudrate=9600)
    client.connect()
    response_part1 = client.read_input_registers(0x00, 50, unit=meters[name])
    if response_part1:
        try:
            voltsp1 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[0:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_V", voltsp1)
            voltsp2 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[2:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_V", voltsp2)
            voltsp3 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[4:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_V", voltsp3)

            ampsp1 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[6:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_A", ampsp1)
            ampsp2 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[8:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_A", ampsp2)
            ampsp3 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[10:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_A", ampsp3)

            activep1 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[12:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_W", activep1)
            activep2 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[14:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_W", activep2)
            activep3 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[16:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_W", activep3)

            app1 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[18:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_VA", app1)
            app2 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[20:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_VA", app2)
            app3 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[22:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_VA", app3)

            rea1 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[24:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_Var", rea1)
            rea2 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[26:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_Var", rea2)
            rea3 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[28:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_Var", rea3)

            pf1 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[30:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_PF", pf1)
            pf2 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[32:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_PF", pf2)
            pf3 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[34:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_PF", pf3)

            ang1 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[36:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_Angle", ang1)
            ang2 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[38:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_Angle", ang2)
            ang3 = BinaryPayloadDecoder.fromRegisters(response_part1.registers[40:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_Angle", ang3)

            avgv = BinaryPayloadDecoder.fromRegisters(response_part1.registers[42:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Average_V", avgv)
            avga = BinaryPayloadDecoder.fromRegisters(response_part1.registers[46:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Average_A", avga)
            suma = BinaryPayloadDecoder.fromRegisters(response_part1.registers[48:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Sum_A", suma)
        except:
            print("exception in part_1")

    response_part2 = client.read_input_registers(52, 58, unit=meters[name])
    if response_part2:
        try:
            totW = BinaryPayloadDecoder.fromRegisters(response_part2.registers[0:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Total_W", totW)
            totva = BinaryPayloadDecoder.fromRegisters(response_part2.registers[4:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Total_VA", totva)
            totvar = BinaryPayloadDecoder.fromRegisters(response_part2.registers[8:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Total_Var", totvar)

            totpf = BinaryPayloadDecoder.fromRegisters(response_part2.registers[10:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Total_PF", totpf)
            totang = BinaryPayloadDecoder.fromRegisters(response_part2.registers[14:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Total_Angle", totang)
            freq = BinaryPayloadDecoder.fromRegisters(response_part2.registers[18:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Frequency", freq)

            impkwh = BinaryPayloadDecoder.fromRegisters(response_part2.registers[20:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Imported_kWh", impkwh)
            expkwh = BinaryPayloadDecoder.fromRegisters(response_part2.registers[22:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Exported_kWh", expkwh)
            impkvar = BinaryPayloadDecoder.fromRegisters(response_part2.registers[24:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Imported_kVarh", impkvar)

            expkvar = BinaryPayloadDecoder.fromRegisters(response_part2.registers[26:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Exported_kVarh", expkvar)
            kvah = BinaryPayloadDecoder.fromRegisters(response_part2.registers[28:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_kVah", kvah)
            ah = BinaryPayloadDecoder.fromRegisters(response_part2.registers[30:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Ah", ah)

            totsysw = BinaryPayloadDecoder.fromRegisters(response_part2.registers[32:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Total_sys_W", totsysw)
            maxsysw = BinaryPayloadDecoder.fromRegisters(response_part2.registers[34:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Max_sys_W", maxsysw)
            totsysva = BinaryPayloadDecoder.fromRegisters(response_part2.registers[48:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Total_sys_VA", totsysva)

            maxsysva = BinaryPayloadDecoder.fromRegisters(response_part2.registers[50:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Max_sys_VA", maxsysva)
            neutsysa = BinaryPayloadDecoder.fromRegisters(response_part2.registers[52:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Neutral_sys_A", neutsysa)
            maxneutsysa = BinaryPayloadDecoder.fromRegisters(response_part2.registers[56:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Max_neutral_sys_A", maxneutsysa)

        except:
            print("exception in part_2")

    response_part3 = client.read_input_registers(200, 70, unit=meters[name])
    if response_part3:
        try:
            l1l2v = BinaryPayloadDecoder.fromRegisters(response_part3.registers[0:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_L2_V", l1l2v)
            l2l3v = BinaryPayloadDecoder.fromRegisters(response_part3.registers[2:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_L3_V", l2l3v)
            l3l1v = BinaryPayloadDecoder.fromRegisters(response_part3.registers[4:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_L1_V", l3l1v)

            avglv = BinaryPayloadDecoder.fromRegisters(response_part3.registers[6:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Average_LV", avglv)
            neuta = BinaryPayloadDecoder.fromRegisters(response_part3.registers[24:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Neutral_A", neuta)
            l1vthd = BinaryPayloadDecoder.fromRegisters(response_part3.registers[34:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_V_THD", l1vthd)

            l2vthd = BinaryPayloadDecoder.fromRegisters(response_part3.registers[36:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_V_THD", l2vthd)
            l3vthd = BinaryPayloadDecoder.fromRegisters(response_part3.registers[38:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_V_THD", l3vthd)
            l1athd = BinaryPayloadDecoder.fromRegisters(response_part3.registers[40:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_A_THD", l1athd)

            l2athd = BinaryPayloadDecoder.fromRegisters(response_part3.registers[42:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_A_THD", l2athd)
            l3athd = BinaryPayloadDecoder.fromRegisters(response_part3.registers[44:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_A_THD", l3athd)
            avgvthd = BinaryPayloadDecoder.fromRegisters(response_part3.registers[48:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Average_V_THD", avgvthd)

            avgathd = BinaryPayloadDecoder.fromRegisters(response_part3.registers[50:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Average_A_THD", avgathd)
            l1ad = BinaryPayloadDecoder.fromRegisters(response_part3.registers[58:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_A_demand", l1ad)
            l2ad = BinaryPayloadDecoder.fromRegisters(response_part3.registers[60:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_A_demand", l2ad)

            l3ad = BinaryPayloadDecoder.fromRegisters(response_part3.registers[62:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_A_demand", l3ad)
            maxl1ad = BinaryPayloadDecoder.fromRegisters(response_part3.registers[64:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Max_L1_A_demand", maxl1ad)
            maxl2ad = BinaryPayloadDecoder.fromRegisters(response_part3.registers[66:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Max_L2_A_demand", maxl2ad)
            maxl3ad = BinaryPayloadDecoder.fromRegisters(response_part3.registers[68:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Max_L3_A_demand", maxl3ad)

        except:
            print("exception in part_3")

    response_part4 = client.read_input_registers(334, 12, unit=meters[name])
    if response_part4:
        try:
            l1l2vthd = BinaryPayloadDecoder.fromRegisters(response_part4.registers[0:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L1_L2_V_THD", l1l2vthd)
            l2l3vthd = BinaryPayloadDecoder.fromRegisters(response_part4.registers[2:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L2_L3_V_THD", l2l3vthd)
            l3l1vthd = BinaryPayloadDecoder.fromRegisters(response_part4.registers[4:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_L3_L1_V_THD", l3l1vthd)

            avgllvthd = BinaryPayloadDecoder.fromRegisters(response_part4.registers[6:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_Average_LL_V_THD", avgllvthd)
            kwh = BinaryPayloadDecoder.fromRegisters(response_part4.registers[8:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_kWh", kwh)
            kVarh = BinaryPayloadDecoder.fromRegisters(response_part4.registers[10:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_kVarh", kVarh)
        except:
            print("exception in part_4")

    write_to_influx()

for name in air_conditioners:
    try:
        client = ModbusClient(method='rtu', port='/dev/ttyUSB0', timeout=0.3, baudrate=9600)
        client.connect()
        response = client.read_holding_registers(51, 1, unit=air_conditioners[name])
        if response:
            result = BinaryPayloadDecoder.fromRegisters(response.registers[0:], byteorder=Endian.Big).decode_16bit_int()
            append_to_series(name + "_switch_binary", result)
        write_to_influx()
    except:
        print(name, "exception")

for name in thermometers:
    try:
        client = ModbusClient(method='rtu', port='/dev/ttyUSB0', timeout=0.3, baudrate=9600)
        client.connect()
        response = client.read_input_registers(96, 4, unit=thermometers[name])
        decrement = 3
        if thermometers[name] == 64:
            decrement = 4
        if response:
            hum = BinaryPayloadDecoder.fromRegisters(response.registers[0:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_sensor_humidity", hum)
            temp = BinaryPayloadDecoder.fromRegisters(response.registers[2:], byteorder=Endian.Big).decode_32bit_float()
            append_to_series(name + "_sensor_temperature", temp - decrement)
        write_to_influx()
    except:
        print(name, "exception")


for name in heatmeters:
    try:
        client = ModbusClient(method='rtu', port='/dev/ttyUSB0', timeout=2, baudrate=9600, stopbits=1, parity='E')
        client.connect()
        response = client.read_holding_registers(1, 20, unit=1)
        if response:
            temp1 = BinaryPayloadDecoder.fromRegisters(response.registers[0:], byteorder=Endian.Big)
            temp2 = BinaryPayloadDecoder.fromRegisters(response.registers[1:], byteorder=Endian.Big)
            currentFlow = BinaryPayloadDecoder.fromRegisters(response.registers[3:], byteorder=Endian.Big)
            currentPower = BinaryPayloadDecoder.fromRegisters(response.registers[5:], byteorder=Endian.Big)
            energy = BinaryPayloadDecoder.fromRegisters(response.registers[13:], byteorder=Endian.Big)
            volume = BinaryPayloadDecoder.fromRegisters(response.registers[15:], byteorder=Endian.Big)
            append_to_series(name + "_temp_hot_side", temp1.decode_16bit_int() * 0.01)
            append_to_series(name + "_temp_cold_side", temp2.decode_16bit_int() * 0.01)
            append_to_series(name + "_current_flow", currentFlow.decode_32bit_int() * 0.001)
            append_to_series(name + "_current_power", currentPower.decode_32bit_int() * 0.01)
            append_to_series(name + "_energy", energy.decode_32bit_uint() * 0.01)
            append_to_series(name + "_volume", volume.decode_32bit_uint() * 0.01)
            write_to_influx()
    except:
        print(name, "exception")


for item in air_conditioners:
    try:
        client = InfluxClient()
        modbus_client = ModbusClient(method='rtu', port='/dev/ttyUSB0', timeout=2, baudrate=9600)
        modbus_client.connect()
        last_set_val = client.get_last_measurement_value(item + "_set_value")
        last_val = client.get_last_measurement_value(item + "_switch_binary")
        print(last_val[0])
        print(last_set_val[0])
        if last_set_val[0] != last_val[0]:
            print("Sending {} {} ..... ".format((int)(last_set_val[0]), item))
            modbus_client.write_registers(51, (int)(last_set_val[0]), unit=air_conditioners[item])
    except:
        print(item, "exception")