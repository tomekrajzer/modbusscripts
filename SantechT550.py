#!/usr/bin/python3
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder

influx = {
    'PORT': 8086,
    'PASSWORD': 'itg2xyCDPV2DyrS2zx',
    'USER': 'writer',
    'HOST': 'localhost',
    'DATABASE': 'data'
}

from datetime import datetime
from influxdb import InfluxDBClient


class InfluxClient():
    def __init__(self):
        self.client = InfluxDBClient(host=influx['HOST'], port=influx['PORT'], username=influx['USER'], password=influx['PASSWORD'],
                                   database=influx['DATABASE'])

    def write_series(self, series):
        print(series)
        self.client.write_points([series])
        print("Succesfully writed to influx DB")



class InfluxMeasurement:
    def __init__(self, measurement_name, value, tags={}):
        self.measurement = measurement_name
        self.fields = {
            "value": float(value)
        }
        self.tags = tags
        self.time = datetime.strftime(datetime.utcnow(), '%Y-%m-%dT%H:%M:%SZ')



def main():
	client = ModbusClient(method='rtu', port='/dev/ttyUSB0', timeout=2, baudrate=9600, stopbits=1, parity='E')
	client.connect()
	response = client.read_holding_registers(1, 20, unit=1)
	if response:
		print(response)
		temp1 = BinaryPayloadDecoder.fromRegisters(response.registers[0:], byteorder=Endian.Big)
		temp2 = BinaryPayloadDecoder.fromRegisters(response.registers[1:], byteorder=Endian.Big)
		# tempDiff = BinaryPayloadDecoder.fromRegisters(response.registers[2:], byteorder=Endian.Big)
		currentFlow = BinaryPayloadDecoder.fromRegisters(response.registers[3:], byteorder=Endian.Big)
		currentPower = BinaryPayloadDecoder.fromRegisters(response.registers[5:], byteorder=Endian.Big)
		# ecold = BinaryPayloadDecoder.fromRegisters(response.registers[7:], byteorder=Endian.Big)
		# time = BinaryPayloadDecoder.fromRegisters(response.registers[9:], byteorder=Endian.Big)
		# infoCode = BinaryPayloadDecoder.fromRegisters(response.registers[11:], byteorder=Endian.Big)
		# moduleInfo = BinaryPayloadDecoder.fromRegisters(response.registers[12:], byteorder=Endian.Big)
		# energy = BinaryPayloadDecoder.fromRegisters(response.registers[13:], byteorder=Endian.Big)
		volume = BinaryPayloadDecoder.fromRegisters(response.registers[15:], byteorder=Endian.Big)
		# serialNumber = BinaryPayloadDecoder.fromRegisters(response.registers[17:], byteorder=Endian.Big)


		# print(temp1.decode_16bit_int())
		# print(temp2.decode_16bit_int())
		# print(currentFlow.decode_32bit_int())
		# print(currentPower.decode_32bit_int())
		# print(volume.decode_32bit_uint())

		write_to_influx(f"heatmeter1_temp_hot_side", temp1.decode_16bit_int() * 0.01)
		write_to_influx(f"heatmeter1_temp_cold_side", temp2.decode_16bit_int() * 0.01)
		write_to_influx(f"heatmeter1_current_flow", currentFlow.decode_32bit_int() * 0.01)
		write_to_influx(f"heatmeter1_current_power", currentPower.decode_32bit_int())
		write_to_influx(f"heatmeter1_volume", volume.decode_32bit_uint())


def write_to_influx(measurement_name, value):
	influx = InfluxClient()
	measurement = InfluxMeasurement(measurement_name, value)
	influx.write_series(vars(measurement))


if __name__ == '__main__':
	main()