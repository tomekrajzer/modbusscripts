#!/usr/bin/python3
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder

for address in range(1, 5):
	client = ModbusClient(method='rtu', port='/dev/ttyUSB0', timeout=0.3, baudrate='9600')
	client.connect()
	response = client.read_input_registers(0x00, 12, unit=address)
	if response:
		try:
			voltsp1 = BinaryPayloadDecoder.fromRegisters(response.registers[0:], byteorder=Endian.Big)
			voltsp2 = BinaryPayloadDecoder.fromRegisters(response.registers[2:], byteorder=Endian.Big)
			voltsp3 = BinaryPayloadDecoder.fromRegisters(response.registers[4:], byteorder=Endian.Big)

			ampsp1 = BinaryPayloadDecoder.fromRegisters(response.registers[6:], byteorder=Endian.Big)
			ampsp2 = BinaryPayloadDecoder.fromRegisters(response.registers[8:], byteorder=Endian.Big)
			ampsp3 = BinaryPayloadDecoder.fromRegisters(response.registers[10:], byteorder=Endian.Big)

			print("Device address: " + str(address) + "\n" +
				"L1:" + str("%.2f" % voltsp1.decode_32bit_float()) + "V " + str(
				"%.2f" % ampsp1.decode_32bit_float()) + "A \n"
				"L2:" + str("%.2f" % voltsp2.decode_32bit_float()) + "V " + str(
				"%.2f" % ampsp2.decode_32bit_float()) + "A \n" +
				"L3:" + str("%.2f" % voltsp3.decode_32bit_float()) + "V " + str(
				"%.2f" % ampsp3.decode_32bit_float()) + "A \n")
		except:
			print("error in address {address}".format(address=address))
